#!/bin/bash

# /etc/httpd/conf.d/ 

DOMAIN=$1
PHP_VER=$2
WEBROOT=$3
WEBUSER='kollarj'

APACHE_CONFIG_DIRECTORY="/etc/httpd/conf.d"
PHP_CONFIG_DIRECTORY="/etc/opt/remi/php${PHP_VER}/php-fpm.d"
PHP_SOCKET_DIRECTORY="/var/opt/remi/php${PHP_VER}/run/php-fpm"

mkdir -p ${WEBROOT}/web
mkdir -p ${WEBROOT}/moodledata

chown -R ${WEBUSER}:${WEBUSER} ${WEBROOT}

sudo tee ${APACHE_CONFIG_DIRECTORY}/${DOMAIN}.conf  << END
<Directory $WEBROOT>
	AllowOverride None
	Require all denied
</Directory>

<VirtualHost *:80>
	ServerName $DOMAIN
	DocumentRoot $WEBROOT/web
	ServerAdmin webmaster@$DOMAIN

	#ErrorLog $WEBROOT/${DOMAIN}_error.log
	#CustomLog $WEBROOT/${DOMAIN}_access.log combined

	#<FilesMatch "\.(cgi|shtml|phtml|php)$">
	#	SSLOptions +StdEnvVars
	#</FilesMatch>

	<Directory $WEBROOT/moodledata>
		deny from all
		AllowOverride None
	</Directory>

	<Directory $WEBROOT/web>
		# Clear PHP settings of this website
		<FilesMatch ".+\.ph(p[345]?|t|tml)$">
		SetHandler None
		</FilesMatch>
		Options +FollowSymLinks
		AllowOverride None
		Require all granted
	</Directory>

	# suexec enabled
	<IfModule mod_suexec.c>
		SuexecUserGroup $WEBUSER $WEBUSER
	</IfModule>

	<IfModule mod_proxy_fcgi.c>
		<Directory $WEBROOT/web>
			<FilesMatch "\.php[345]?$">
				SetHandler "proxy:unix:${PHP_SOCKET_DIRECTORY}/${DOMAIN}.sock|fcgi://localhost"
			</FilesMatch>
		</Directory>
	</IfModule>

</VirtualHost>
END


sudo tee ${PHP_CONFIG_DIRECTORY}/${DOMAIN}.conf  << END
[$DOMAIN]
user = $WEBUSER
group = $WEBUSER
listen = ${PHP_SOCKET_DIRECTORY}/${DOMAIN}.sock
listen.owner = apache
listen.group = apache
php_admin_value[disable_functions] = passthru,system
php_admin_flag[allow_url_fopen] = off
pm = dynamic
pm.max_children = 5
pm.start_servers = 2
pm.min_spare_servers = 1
pm.max_spare_servers = 3
chdir = /
END

#echo "127.0.0.1   $DOMAIN     # webDir: $WEBROOT" | sudo tee -a /etc/hosts > /dev/null

sudo chown ${WEBUSER}:${WEBUSER} $WEBROOT
#sudo chmod 770 $WEBROOT

sudo systemctl restart httpd.service
sudo systemctl restart php${PHP_VER}-php-fpm.service
